# wp-wpdesk-composer

##Installation

composer require --dev wpdesk/wp-wpdesk-composer
composer require --dev wpdesk/wp-plugin-flow ^2.0

## Usage

Add to `composer.json` to scripts section:

```$json
        "tests-unit": "docker-compose -f vendor/wpdesk/wp-wpdesk-composer/docker/docker-compose.yaml run wordpress phpunit --no-coverage -c /opt/project/phpunit-unit.xml",
        "tests-integration": "docker-compose -f vendor/wpdesk/wp-wpdesk-composer/docker/docker-compose.yaml run wordpress phpunit --no-coverage -c /opt/project/phpunit-integration.xml",
        "tests": [
            "@composer tests-unit",
            "@composer tests-integration"
        ]
```

To run scripts type in command line:
1. Set plugin date in plugin file:
`composer set-plugin-date`
2. To set plugin contributors:
`composer set-contributors`
3. To set WordPress tested up version number:
`composer replace-tested-up-wp`
4. To set WordPress requires at least version number:
`composer replace-requires-at-least-wp`
5. To set WooCommerce tested up version number:
`composer replace-tested-up-wc`
6. To set WooCommerce requires at least version number:
`composer replace-requires-at-least-wc`
7. To set PHP requires version number:
`composer replace-requires-php`
8. To run commands 1-4:
`composer set-plugin-data`
9. To run unit tests:
`composer run-unit-tests`
or
`composer run-unit-tests fast`
10. To run integration tests:
`composer run-integration-tests`
or
`composer run-integration-tests`
11. To generate .pot files:
`composer generate-pot`
12. To merge translations:
`composer merge-translations`
13. To minify assets
`composer minify-assets`
14. To generate release folder and zip:
`composer generate-plugin-release`
15. To increase plugin versions:
```
composer increase-plugin-version
composer increase-plugin-version --minor
composer increase-plugin-version --major
```

## Translations

Configuration for language commands ( `composer generate-pot`, `composer merge-translations` ) must be entered in composer.json file in `extra`.

Example configuration:
```
	"extra" : {
		"text-domain": "flexible-shipping",
		"translations-folder": "lang",
		"po-files": {
			"pl_PL": "pl_PL.po"
		}
	}
```

`text-domain`: plugin/library text domain

`translations-folder`: folder, where translations are stored

`po-files`: translations files

## Minification

Configuration for assets minification ( `composer minify-assets` ) must be entered in composer.json file in `extra`.

Example configuration:
```
	"extra": {
		"minify-assets" : {
			"js" : [
				"assets/js/admin.js",
				"assets/js/admin_order.js",
				"assets/js/admin_settings.js",
				"assets/js/checkout.js",
				"assets/js/i18n-support.js"
			],
			"css" : [
				"assets/css/admin.css",
				"assets/css/checkout.css"
			]
		}
	}
```

## Additional

To regenerate WooCommerce stubs use ../vendor/wpdesk/wp-wpdesk-composer/director/vendor/szepeviktor/phpstan-wordpress/stub-generators/wc-generate-stubs.sh in WooCommerce dir.

## Faker

Faker can fake libraries in vendor_prefixed libraries.
Faker can change files content, files names and folder names.

Configuration for faker must be entered in composer.json file in `extra`.

Faker runs right after prefixer.

Example configuration:
```
	"extra": {
		"fake-vendor-prefixed": {
			"path": "wpdesk",
			"fake-path": "octolize",
			"content": {
				"wpdesk": "octolize",
				"WPDesk": "Octolize"
			},
			"file-names": {
				"WPDesk": "Octolize",
				"wpdesk": "octolize"
			},
			"folder-names": {
				"wpdesk": "octolize",
				"WPDesk": "Octolize"
			}
		}
	}
```

`path` - namespace (subfolder) in vendor_prefixed

`fake-path` - new namespace, can be blank

`content` - array of strings to find and replace in content

`file-names` - array of strings to find and replace in file names

`folder-names` - array of string to find and replace in folder names
