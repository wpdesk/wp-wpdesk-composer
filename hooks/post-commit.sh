#!/bin/bash

# Do amend when .commit file exists. This means that the commit was executed and some files has been changed
if [ -a .commit ]
    then
    rm .commit
    git add readme.txt
    git add *.php
    git commit --amend -C HEAD --no-verify
fi