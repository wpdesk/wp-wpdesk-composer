#!/bin/bash

# TODO: check if composer.lock is old

FILES=$(git diff --name-only --cached --diff-filter=ACMR)

# Ensure that code quality is like a charm
# PHP lint the files
echo "Checking files with PHP Lint..."
for FILE in $FILES
do
    if [ -f $FILE ]; then
        php -l -d display_errors=0 $FILE
        if [ $? != 0 ]
        then
            echo -e "\e[1;31m\tBasic lint check failed. Fix your code! Aborting commit.\e[0m" >&2
            exit 1
        fi
    fi
done || exit $?

# PHPUnit
echo "Running unit tests..."
vendor/bin/phpunit --configuration phpunit-unit.xml --no-coverage
if [ $? -ne 0 ]; then
  echo -e "\e[1;31m\tUnit tests failed ! Aborting commit.\e[0m" >&2
  exit 1;
fi

# PHP CS
if [ -f phpcs.xml.dist ]; then
  echo "Running Code Sniffer."
  ./vendor/bin/phpcs --encoding=utf-8 -n -p $FILES
fi

if [ $? != 0 ]
then
    echo -e "\e[1;31m\tPHPCS check failed. Fix your code! Aborting commit.\e[0m" >&2
    echo "Run ./vendor/bin/phpcbf for automatic fix or fix it manually."
    exit 1
fi


# PHPStan
if [ -f phpstan.neon.dist ]; then
	if [ ! -f vendor/bin/phpstan ]; then
		./vendor/wpdesk/wp-wpdesk-composer/director/vendor/bin/phpstan --memory-limit=-1 analyse
		echo -e "\e[1;31m\tDetected usage of deprecated PHPStan installation. Please, run 'composer require --dev wpdesk/phpstan-rules'.\e[0m"
	else
		vendor/bin/phpstan --memory-limit=-1 analyse
	fi
    if [ $? -ne 0 ]; then
      echo -e "\e[1;31m\tPHPStan check failed! Aborting commit.\e[0m" >&2
      exit 1;
    fi
fi
