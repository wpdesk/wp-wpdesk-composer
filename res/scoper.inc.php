<?php

declare( strict_types=1 );

$original_config = require getcwd() . DIRECTORY_SEPARATOR . 'scoper.inc.php';

// Migrate original scoper configuration to newer scheme on the fly if required.
$original_config['expose-global-constants'] = $original_config['whitelist-global-constants'] ?? true;
$original_config['expose-global-classes'] = $original_config['whitelist-global-classes'] ?? true;
$original_config['expose-global-functions'] = $original_config['whitelist-global-functions'] ?? true;

if (isset($original_config['files-whitelist'])) {
	$original_config['exclude-files'] = array_map(
		fn (string $file) => str_starts_with($file, '/') ? $file : getcwd() . '/' . $file,
		$original_config['files-whitelist']
	);
}

unset($original_config['whitelist-global-constants'], $original_config['whitelist-global-classes'], $original_config['whitelist-global-functions'], $original_config['files-whitelist'], $original_config['whitelist']);

function getWpExcludedSymbols(string $fileName): array
{
    $filePath = getcwd().'/vendor/sniccowp/php-scoper-wordpress-excludes/generated/'.$fileName;

    return json_decode(
        file_get_contents($filePath),
        true,
    );
}

function getWcExcludedSymbols(string $fileName): array
{
    $filePath = getcwd().'/vendor/wpdesk/php-scoper-woocommerce-excludes/generated/'.$fileName;

    return json_decode(
        file_get_contents($filePath),
        true,
    );
}

$wp_classes   = getWpExcludedSymbols('exclude-wordpress-classes.json');
$wp_interfaces   = getWpExcludedSymbols('exclude-wordpress-interfaces.json');
$wp_functions = getWpExcludedSymbols('exclude-wordpress-functions.json');
$wp_constants = getWpExcludedSymbols('exclude-wordpress-constants.json');

$wc_classes   = getWcExcludedSymbols('exclude-woocommerce-classes.json');
$wc_interfaces   = getWcExcludedSymbols('exclude-woocommerce-interfaces.json');
$wc_functions = getWcExcludedSymbols('exclude-woocommerce-functions.json');
$wc_constants = getWcExcludedSymbols('exclude-woocommerce-constants.json');

$wc_package_classes   = getWcExcludedSymbols('exclude-woocommerce-packages-classes.json');
$wc_package_functions = getWcExcludedSymbols('exclude-woocommerce-packages-functions.json');
$wc_package_interfaces = getWcExcludedSymbols('exclude-woocommerce-interfaces.json');

return array_merge_recursive(
	[
		'exclude-classes' => array_merge(
			$wp_classes,
			$wp_interfaces,
			$wc_classes,
			$wc_interfaces,
			$wc_package_classes,
			$wc_package_interfaces,
			[
				// Internal WP Desk classes, which should be shared globally.
				'WPDesk_Tracker_Data_Provider',
				'WPDesk_Tracker_Interface',
				'WPDesk_Tracker_Sender',
				'WPDesk\Helper\HelperAsLibrary',
				'WPDesk_Tracker_Factory'
			]
		),
		'exclude-constants' => array_merge(
			$wp_constants,
			$wc_constants
		),
		'exclude-functions' => array_merge(
			$wp_functions,
			$wc_functions,
			$wc_package_functions
		),
	],
	$original_config
);
