<?php

namespace WPDesk\Composer\GitPlugin;

use WPDesk\Composer\GitPlugin\Command\FakeVendorPrefixedCommand;
use WPDesk\Composer\GitPlugin\Command\GeneratePhpStormMeta;
use WPDesk\Composer\GitPlugin\Command\GeneratePotCommand;
use WPDesk\Composer\GitPlugin\Command\GenerateVendorPrefixedCommand;
use WPDesk\Composer\GitPlugin\Command\GeneratePluginReleaseCommand;
use WPDesk\Composer\GitPlugin\Command\IncreasePluginVersionCommand;
use WPDesk\Composer\GitPlugin\Command\MergeTranslationsCommand;
use WPDesk\Composer\GitPlugin\Command\MinifyAssetsCommand;
use WPDesk\Composer\GitPlugin\Command\NpmCommand;
use WPDesk\Composer\GitPlugin\Command\ReplaceContributorsCommand;
use WPDesk\Composer\GitPlugin\Command\ReplacePluginDateCommand;
use WPDesk\Composer\GitPlugin\Command\ReplaceRequiresAtLeastWCCommand;
use WPDesk\Composer\GitPlugin\Command\ReplaceRequiresAtLeastWPCommand;
use WPDesk\Composer\GitPlugin\Command\ReplaceRequiresPHPCommand;
use WPDesk\Composer\GitPlugin\Command\ReplaceTestedWCCommand;
use WPDesk\Composer\GitPlugin\Command\ReplaceTestedWPCommand;
use WPDesk\Composer\GitPlugin\Command\RunPhpUnitIntegrationTestsCommand;
use WPDesk\Composer\GitPlugin\Command\RunPhpUnitUnitTestsCommand;
use WPDesk\Composer\GitPlugin\Command\UpdateWPWCAndAddChangelogCommand;

/**
 * Links plugin commands handlers to composer
 *
 * @package WPDesk\Composer\GitPlugin
 */
class CommandProvider implements \Composer\Plugin\Capability\CommandProvider
{
    public function getCommands()
    {
        return [
            new ReplaceContributorsCommand(),
            new ReplacePluginDateCommand(),
            new ReplaceTestedWCCommand(),
            new ReplaceTestedWPCommand(),
            new ReplaceRequiresAtLeastWCCommand(),
            new ReplaceRequiresAtLeastWPCommand(),
            new ReplaceRequiresPHPCommand(),
            new GenerateVendorPrefixedCommand(),
            new RunPhpUnitUnitTestsCommand(),
            new RunPhpUnitIntegrationTestsCommand(),
            new MergeTranslationsCommand(),
            new GeneratePotCommand(),
            new MinifyAssetsCommand(),
            new GeneratePluginReleaseCommand(),
            new NpmCommand(),
	        new UpdateWPWCAndAddChangelogCommand(),
			new FakeVendorPrefixedCommand(),
	        new GeneratePhpStormMeta(),
	        new IncreasePluginVersionCommand(),
        ];
    }
}
