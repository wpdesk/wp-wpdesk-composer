<?php

namespace WPDesk\Composer\GitPlugin;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\Capable;
use Composer\EventDispatcher\Event;
use Composer\Plugin\PluginInterface;
use Composer\Script\ScriptEvents;
use RuntimeException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use WPDesk\Composer\GitPlugin\Command\MinifyAssetsCommand;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;

/**
 * Main plugin class - initializes everything.
 *
 * @package WPDesk\Composer\GitPlugin
 */
class Plugin implements PluginInterface, Capable, EventSubscriberInterface
{
    use ExecuteCommandTrait;

    const TEXT_DOMAIN = 'text-domain';
    const TRANSLATIONS_FOLDER = 'translations-folder';

    const PRIORITY_RUN_FIRST  = 10;
    const PRIORITY_RUN_SECOND = 9;
    const PRIORITY_RUN_THIRD  = 8;

	/**
     * @var Composer
     */
    private $composer;

    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ScriptEvents::PRE_AUTOLOAD_DUMP => [
                ['prepareToDump', 0],
            ],
            ScriptEvents::POST_INSTALL_CMD => [
                ['minifyAssets', self::PRIORITY_RUN_FIRST],
                ['onDependenciesChangedEvent', self::PRIORITY_RUN_SECOND],
                ['mergeTranslations', self::PRIORITY_RUN_THIRD],
            ],
            ScriptEvents::POST_UPDATE_CMD => [
                ['minifyAssets', self::PRIORITY_RUN_FIRST],
                ['onDependenciesChangedEvent', self::PRIORITY_RUN_SECOND],
                ['mergeTranslations', self::PRIORITY_RUN_THIRD],
	            ['generatePhpStormMeta',self::PRIORITY_RUN_THIRD]
            ]
        ];
    }

    /**
     * Preparation to composer autoloader dump
     */
    public function prepareToDump()
    {
        $prefixed_dir = 'vendor_prefixed';
        if ($this->isProjectUsePrefixer() && !is_dir($prefixed_dir)) {
            mkdir($prefixed_dir, 0777, true);
        }
    }

    /**
     * @inheritDoc
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    /**
     * @inheritDoc
     */
    public function deactivate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    /**
     * @inheritDoc
     */
    public function uninstall(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;

        if ($this->isWPDeskComposerInstalled()) {
            $this->removeHooks();
        }
    }

    /**
     * @inheritDoc
     */
    public function getCapabilities(): array
    {
        return [
            \Composer\Plugin\Capability\CommandProvider::class => CommandProvider::class
        ];
    }

    /**
     * @return bool
     */
    public function isWPDeskComposerInstalled()
    {
        return file_exists(getcwd() . '/vendor/wpdesk/wp-wpdesk-composer');
    }

    /**
     * Entry point for post install and post update events.
     *
     * @throws RuntimeException
     * @throws ProcessFailedException
     */
    public function onDependenciesChangedEvent()
    {
        $this->installHooks();
        if ($this->isWPDeskComposerInstalled()) {
            $this->runPrefixerAndDump();
            $this->runNpm();
        } else {
            $this->removeHooks();
        }
    }

    /**
     * Merge translations.
     *
     * @param Event $event .
     */
    public function mergeTranslations(Event $event)
    {
        if ($this->isWPDeskComposerInstalled()) {
            $extra = $this->composer->getPackage()->getExtra();
            if (isset($extra[self::TEXT_DOMAIN]) && $extra[self::TRANSLATIONS_FOLDER]) {
                passthru("composer generate-pot");
                passthru("composer merge-translations");
                passthru("composer generate-pot");
            }
        }
    }

    /**
     * Minify assets.
     *
     * @param Event $event .
     */
    public function minifyAssets(Event $event)
    {
        if ($this->isWPDeskComposerInstalled()) {
            $extra = $this->composer->getPackage()->getExtra();
            if (isset($extra[MinifyAssetsCommand::MINIFY_ASSETS])) {
                passthru("composer minify-assets");
            }
        }
    }

	/**
	 * Merge translations.
	 *
	 * @param Event $event .
	 */
	public function generatePhpStormMeta(Event $event)
	{
		if ($this->isWPDeskComposerInstalled()) {
			$extra = $this->composer->getPackage()->getExtra();
			if (isset($extra[self::TEXT_DOMAIN])) {
				passthru("composer generate-phpstorm-meta");
			}
		}
	}

	/**
     * Installs git hooks
     */
    private function installHooks()
    {
        $targetDir = $this->getHooksDir();
        $this->prepareGitDir($targetDir);

        foreach ($this->getHookFiles() as $file) {
            $this->copyHookFile($file, $targetDir);
        }
    }

    /**
     * Returns git hooks dir. Supporting git worktrees
     *
     * @return string
     */
    private function getHooksDir()
    {
        $gitFile = getcwd() . DIRECTORY_SEPARATOR . '.git';
        if (file_exists($gitFile) && !is_dir($gitFile)) {
            // Offset by 8 to get full path. File starts with gitdir:
            $worktreeRoot = trim(file_get_contents($gitFile, false, null, 8));
            return $worktreeRoot . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'hooks';
        }
        return getcwd() . DIRECTORY_SEPARATOR . '.git' . DIRECTORY_SEPARATOR . 'hooks';
    }

    /**
     * Makes sure that git hooks dir exists
     *
     * @param $targetDir
     */
    private function prepareGitDir($targetDir)
    {
        if (!is_dir($targetDir)) { // this if cannot be merged
            if (!mkdir($targetDir, 0775, true) && !is_dir($targetDir)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $targetDir));
            }
        }
    }

    /**
     * Return all git hook files to replace/remove
     *
     * @return string[]
     */
    private function getHookFiles()
    {
        return [
            'pre-commit',
            'post-commit'
        ];
    }

    /**
     * Copy hook files into git hook dir
     *
     * @param string $file
     * @param string $targetDir
     */
    private function copyHookFile($file, $targetDir)
    {
        $sourceFile = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'hooks' . DIRECTORY_SEPARATOR . $file . '.sh';
        $targetFile = $targetDir . DIRECTORY_SEPARATOR . $file;
        if (!file_exists($targetFile) && file_exists($sourceFile)) {
            copy($sourceFile, $targetFile);
            chmod($targetFile, 0775);
        }
    }

    /**
     * Runs php-scoper and dump autoloader to update vendor_prefixed
     */
    public function runPrefixerAndDump()
    {
        if ($this->isProjectUsePrefixer()) {
            $this->executePassThrough("composer generate-vendor-prefixed");
			// $this->executePassThrough("composer fake-vendor-prefixed");
            $this->executePassThrough("composer dump");
        }
    }

    /**
     * Run composer npm command.
     */
    private function runNpm()
    {
        if ($this->verifyCommand('npm')) {
            $this->executePassThrough('composer npm');
        }
    }

    /**
     * @param string $command .
     *
     * @return bool
     */
    private function verifyCommand($command)
    {
        $windows = strpos(PHP_OS, 'WIN') === 0;
        // TODO: temporary fix for windows, currently not working as expected
        if ($windows) {
            return true;
        }
        $test = $windows ? 'where' : 'command -v';
        return is_executable(trim(shell_exec("$test $command")));
    }

    /**
     * @return bool
     */
    private function isProjectUsePrefixer()
    {
        return file_exists('scoper.inc.php');
    }

    /**
     * Remove git hooks
     */
    private function removeHooks()
    {
        $targetDir = $this->getHooksDir();

        foreach ($this->getHookFiles() as $file) {
            $targetFile = $targetDir . DIRECTORY_SEPARATOR . $file;
            if (file_exists($targetFile)) {

                unlink($targetFile);
            }
        }
    }
}
