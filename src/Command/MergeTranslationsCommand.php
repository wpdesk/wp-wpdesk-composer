<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Composer\Package\PackageInterface;
use Gettext\Merge;
use Gettext\Translation;
use Gettext\Translations;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\LibraryFaker\Faker;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\DirectoriesTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Can merge plugin translations.
 * Translations will be merged from plugin .pot file, plugin .po file an library .po files.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class MergeTranslationsCommand extends BaseCommand
{
    use SedTrait;
    use ColorOutputTrait;
    use DirectoriesTrait;
    use ExecuteCommandTrait;

    const TEXT_DOMAIN = 'text-domain';
    const PO_FILES = 'po-files';
    const TRANSLATIONS_FOLDER = 'translations-folder';

    protected function configure()
    {
        $this
            ->setName('merge-translations')
            ->setDescription('Merges translations from libraries to plugin. There must be configuration in composer.json extra section. See readme file.');
    }

    /**
     * Maybe replace text domain in library files.
     *
     * @param string $working_directory
     * @param string $plugin_text_domain
     * @param string $library_text_domain
     * @param string $library_path
     */
    private function maybeReplaceLibraryTextDomain($working_directory, $plugin_text_domain, $library_text_domain, $library_path)
    {
        echo "Replacing text domain on "
            . $this->addColorToOutput($this->removeCwdFromPath($library_path, $working_directory))
            . " from $library_text_domain to $plugin_text_domain\n";
        $pattern = "/\'$library_text_domain\'/";
        $replace = "'$plugin_text_domain'";
        foreach ($this->getDirContents($library_path) as $dir) {
            $file_pattern = $dir . '/*.php';
            $this->file_regex_replace($file_pattern, $pattern, $replace);
        }
    }

    /**
     * Maybe clear old translations files.
     *
     * @param string $working_directory
     * @param array $plugin_po_files
     * @param string $translations_folder
     * @param string $plugin_text_domain
     */
    private function maybeClearOldTranslationsFiles($working_directory, array $plugin_po_files, $translations_folder, $plugin_text_domain)
    {
        foreach ($plugin_po_files as $language => $po_file) {
            $po_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '-' . $language . '.po';
            if (file_exists($po_file)) {
                echo "Deleting " . $this->addColorToOutput($this->removeCwdFromPath($po_file, $working_directory)) . "\n";
                unlink($po_file);
            }
            $mo_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '-' . $language . '.mo';
            if (file_exists($mo_file)) {
                echo "Deleting " . $this->addColorToOutput($this->removeCwdFromPath($mo_file, $working_directory)). "\n";
                unlink($mo_file);
            }
        }
    }

    /**
     * Create po files from pot files.
     *
     * @param string $working_directory
     * @param array $plugin_po_files
     * @param string $translations_folder
     * @param string $plugin_text_domain
     */
    private function createPoFromPot($working_directory, array $plugin_po_files, $translations_folder, $plugin_text_domain)
    {
        foreach ($plugin_po_files as $language => $po_file) {
            $source_file = $working_directory . '/' . $translations_folder . '/' . $po_file;
            $pot_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '.pot';
            $destination_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '-' . $language . '.po';
            echo "Creating " . $this->addColorToOutput($this->removeCwdFromPath($destination_file, $working_directory))
                . " from " . $this->addColorToOutput($this->removeCwdFromPath($pot_file, $working_directory)) . "\n";
            if (!file_exists($pot_file)) {
                throw new \RuntimeException(
                    $this->addColorToOutput("Fatal: source .pot file $pot_file not exists!", '0;31')
                );
            }
            $translations_pot = Translations::fromPoFile($pot_file);
            $translations = Translations::fromPoFile($source_file);
            $translations = $this->createTranslationsWithoutDisabled($translations);
            $translations_pot->mergeWith($translations, Merge::REFERENCES_OURS | Merge::ADD | Merge::TRANSLATION_OVERRIDE);
            $translations_pot->setHeader('Language', $language);
            $translations_pot->toPoFile($destination_file);
        }
    }

    /**
     * Remove disabled translation.
     *
     * @param Translations $translations .
     *
     * @return Translations
     */
    private function createTranslationsWithoutDisabled(Translations $translations)
    {
        $translations_without_disabled = new Translations();
        $headers = $translations->getHeaders();
        foreach ($headers as $name => $value) {
            $translations_without_disabled->setHeader($name, $value);
        }

        /**
         * @var  $item Translation
         */
        foreach ($translations as $key => $item) {
            if (!$item->isDisabled()) {
                $translations_without_disabled[$key] = $item;
            }
        }
        return $translations_without_disabled;
    }

    /**
     * Append library translations to plugin translations.
     *
     * @param string $working_directory
     * @param array $plugin_po_files
     * @param string $translations_folder
     * @param string $plugin_text_domain
     * @param string $repository_name
     * @param string $repository_translations_folder
     * @param array $library_po_files
     */
    private function appendLibraryTranslations(
        $working_directory,
        array $plugin_po_files,
        $translations_folder,
        $plugin_text_domain,
        $repository_name,
        $repository_translations_folder,
        array $library_po_files
    ) {
        foreach ($plugin_po_files as $language => $po_file) {
            if (isset($library_po_files[$language])) {
                $destination_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '-' . $language . '.po';
                $library_file = $working_directory . '/vendor/' . $repository_name . '/' . $repository_translations_folder . '/' . $library_po_files[$language];

                echo "Merging " . $this->addColorToOutput($this->removeCwdFromPath($library_file, $working_directory))
                    . " to " . $this->addColorToOutput($this->removeCwdFromPath($destination_file, $working_directory)) . "\n";
                $translations = Translations::fromPoFile($destination_file);
                $library_translations = Translations::fromPoFile($library_file);
                $library_translations = $this->createTranslationsWithoutDisabled($library_translations);
                $translations->mergeWith($library_translations, Merge::DEFAULTS);
                $translations->toPoFile($destination_file);
            }
        }
    }

    /**
     * Create mo translation files.
     *
     * @param string $working_directory
     * @param array $plugin_po_files
     * @param string $translations_folder
     * @param string $plugin_text_domain
     */
    private function createMo($working_directory, array $plugin_po_files, $translations_folder, $plugin_text_domain)
    {
        foreach ($plugin_po_files as $language => $po_file) {
            $source_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '-' . $language . '.po';
            $destination_file = $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '-' . $language . '.mo';
            echo "Creating " . $this->addColorToOutput($this->removeCwdFromPath($destination_file, $working_directory))
                . " from " . $this->addColorToOutput($this->removeCwdFromPath($source_file, $working_directory)) . "\n";
            $translations = Translations::fromPoFile($source_file);
            $translations->toMoFile($destination_file);
        }
    }

    /**
     * Maybe append libraries translations.
     *
     * @param string $working_directory
     * @param array $plugin_po_files
     * @param string $translations_folder
     * @param string $plugin_text_domain
     */
    private function maybeAppendLibrariesTranslations($working_directory, array $plugin_po_files, $translations_folder, $plugin_text_domain)
    {
        $repositories = $this->getComposer()->getRepositoryManager()->getLocalRepository()->getCanonicalPackages();

        foreach ($repositories as $repository) {
            /** @see README.md for extra section info. **/
            $library_extra = $repository->getExtra();
            if (isset($library_extra[self::TEXT_DOMAIN])) {
                $library_text_domain = $library_extra[self::TEXT_DOMAIN];
                $library_po_files =
                    isset($library_extra[self::PO_FILES]) ? $library_extra[self::PO_FILES] : array();
                $prefixed_library_path = $this->getPrefixedLibraryPath($working_directory, $repository);
                $repository_translations_folder = $library_extra[ self::TRANSLATIONS_FOLDER ];

                $this->maybeReplaceLibraryTextDomain($working_directory, $plugin_text_domain, $library_text_domain, $prefixed_library_path);

                $this->appendLibraryTranslations(
                    $working_directory,
                    $plugin_po_files,
                    $translations_folder,
                    $plugin_text_domain,
                    $repository->getName(),
                    $repository_translations_folder,
                    $library_po_files
                );
            }
        }
    }

	private function preparePrefixedRepositoryName($repository) {
		$extra = $this->getComposer()->getPackage()->getExtra();
		if (isset($extra['fake-vendor-prefixed'])) {
			$faker = Faker::createFromConfig($extra['fake-vendor-prefixed']);
			$repository = $faker->fakePrefixedRepositoryName($repository);
		}

		return $repository;
	}

	/**
	 * @param string $working_directory
	 * @param string $translations_folder
	 * @param string $plugin_text_domain
	 * @param array  $plugin_po_files
	 */
	private function prepareJavascriptTranslations($translations_folder, $plugin_text_domain, $plugin_po_files) {
		foreach ( $plugin_po_files as $lang => $po_file ) {
			$po_file = $translations_folder . '/' . $plugin_text_domain . '-' . $lang . '.po';
			echo "Creating JavaScript translations from " . $this->addColorToOutput( $po_file ) . "\n";
			$command = './vendor/bin/wp i18n make-json ' . $po_file . ' --allow-root';
			$this->executePassThrough( $command );
		}
    }


	/**
     * Execute command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @see README.md for extra section info. **/
        $extra = $this->getComposer()->getPackage()->getExtra();

        if (isset($extra[self::TEXT_DOMAIN])) {
            $working_directory = getcwd();
            $translations_folder =
                isset($extra[self::TRANSLATIONS_FOLDER]) ? $extra[self::TRANSLATIONS_FOLDER] : 'lang';
            $plugin_text_domain = $extra[self::TEXT_DOMAIN];
            $plugin_po_files = isset($extra[self::PO_FILES]) ? $extra[self::PO_FILES] : array();

            $this->maybeClearOldTranslationsFiles($working_directory, $plugin_po_files, $translations_folder, $plugin_text_domain);

            $this->createPoFromPot($working_directory, $plugin_po_files, $translations_folder, $plugin_text_domain);

            $this->maybeAppendLibrariesTranslations($working_directory, $plugin_po_files, $translations_folder, $plugin_text_domain);

            $this->createMo($working_directory, $plugin_po_files, $translations_folder, $plugin_text_domain);

            $this->prepareJavascriptTranslations($translations_folder, $plugin_text_domain, $plugin_po_files);

            $this->prepareNativePhpTranslations($translations_folder, $plugin_text_domain, $plugin_po_files);
            return 0;
        }
    }

	private function prepareNativePhpTranslations($translations_folder, $plugin_text_domain, $plugin_po_files) {
		foreach ( $plugin_po_files as $lang => $po_file ) {
			$po_file = $translations_folder . '/' . $plugin_text_domain . '-' . $lang . '.po';
			echo "Creating PHP l10n.php translations from " . $this->addColorToOutput( $po_file ) . "\n";
			$command = './vendor/bin/wp i18n make-php ' . $po_file . ' --allow-root';
			$this->executePassThrough( $command );
		}
    }

    /**
     * php-scoper tries to create the shortest path possible for prefixed assets. This means, that if we prefix libraries from two different vendors, e.g. `wpdesk` and `psr`, it will result in directory structure similar to original composer packages.
     *
     * It is different, if there are just packages from one vendor though. If you prefix just two libraries, e.g. `wpdesk/wp-logs` and `wpdesk/wp-basic-requirements`, it will leave vendor name, landing in:
     * /vendor_prefixed
     *   /wp-logs
     *   /wp-basic-requirements
     *
     * There are also more complex possibilities, which are out of scope for now. Just handling those two cases is enough.
     */
    private function getPrefixedLibraryPath(string $cwd, PackageInterface $package): string
    {
        $prefixed_path = $cwd . '/vendor_prefixed/' . $this->preparePrefixedRepositoryName($package->getName());

        if (file_exists($prefixed_path) ) {
            return $prefixed_path;
        }

        // Second attempt, without vendor name prefix.
        $prefixed_path = $cwd . '/vendor_prefixed/' . $this->preparePrefixedRepositoryName(basename($package->getName()));
        if (file_exists($prefixed_path) ) {
            return $prefixed_path;
        }

        throw new \RuntimeException(sprintf('Could not find a library "%s" in vendor_prefixed folder. Is it added in php-scoper settings?', $package->getName()));
    }

}
