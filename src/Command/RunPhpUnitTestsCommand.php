<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
abstract class RunPhpUnitTestsCommand extends BaseCommand
{

    const FAST = 'fast';

    /**
     * @param string $command
     * @param OutputInterface $output
     */
    protected function execAndOutput($command, OutputInterface $output)
    {
        passthru($command);
    }

    /**
     * Get Doceker Compose command.
     *
     * @return string
     */
    abstract protected function getDockerComposeCommand();

    /**
     * Get service name.
     *
     * @return string
     */
    abstract protected function getServiceName();

    /**
     * Execute command.
     *
     * @param InputInterface $input .
     * @param OutputInterface $output .
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dockerComposeYaml = 'vendor/wpdesk/wp-wpdesk-composer/docker/docker-compose.yaml';
        $fastTest = $input->getArgument(self::FAST);
        $additionalParameters = ' -e PROJECT_NAME="' . basename(getcwd()) . '" ';
        $runTestsCommand = 'docker-compose -f ' . $dockerComposeYaml . ' run ' . $additionalParameters . ' ' . $this->getServiceName() . ' ' . $this->getDockerComposeCommand();
        $output->writeln('Command: ' . $runTestsCommand);
        $this->execAndOutput($runTestsCommand, $output);
        if (empty($fastTest) || self::FAST !== $fastTest) {
            $this->execAndOutput('docker-compose -f ' . $dockerComposeYaml . ' down -v', $output);
        }
		return 0;
    }
}
