<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;

/**
 * Can build WPDesk plugin.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class NpmCommand extends BaseCommand
{

    use ExecuteCommandTrait;
    use ColorOutputTrait;

    protected function configure()
    {
        $this
            ->setName( 'npm' )
            ->setDescription( 'Builds WPDesk plugin.' )
            ->addOption( 'no-dev' );
    }

    /**
     * Execute command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo $this->addColorToOutput( 'Running npm.' ) . "\n";
        $working_directory = getcwd();

        if ( file_exists( $working_directory . '/package.json' ) ) {
            echo $this->addColorToOutput( 'package.json file present.' ) . "\n";
            echo $this->addColorToOutput( 'Running npm ci.' ) . "\n";
            $this->executePassThrough('npm ci');
            echo $this->addColorToOutput( 'Running npm run prod.' ) . "\n";
            $this->executePassThrough('npm run prod');
            echo $this->addColorToOutput( 'npm done.' ) . "\n";
        } else {
            echo $this->addColorToOutput( 'package.json file not present.' ) . "\n";
        }
		return 0;

    }
}
