<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\PluginHeaderParserTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\PluginVersionTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;
use WPDesk\Composer\GitPlugin\Command\Tools\Version;

/**
 * Replaces wp tested up line and wc tested line in the plugin readme file and ads changelog.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class IncreasePluginVersionCommand extends BaseCommand {
	use ExecuteCommandTrait;
	use PluginHeaderParserTrait;
	use PluginVersionTrait;
	use SedTrait;

	const README_TXT    = 'readme.txt';
	const CHANGELOG_TXT = 'changelog.txt';

	protected function configure() {
		$this
			->setName( 'increase-plugin-version' )
			->setDescription( 'Increases plugin version.' )
			->addOption( '--major' )
			->addOption( '--minor' )
			->addOption( '--patch' )
		;
	}

	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return int|void
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$extra = $this->getComposer()->getPackage()->getExtra();

		$plugin_headers = $this->parseHeadersForPlugin();

		$position = 2;
		if ( $input->getOption( 'major' ) ) {
			$position = 0;
		}
		if ( $input->getOption( 'minor' ) ) {
			$position = 1;
		}

		$current_plugin_version   = $plugin_headers['version'];
		$increased_plugin_version = $this->increase_plugin_version( $current_plugin_version, $position );

		$this->replace_plugin_version( $increased_plugin_version );

		$date = date( 'Y-m-d' );

		if ( file_exists( self::README_TXT ) ) {
			$this->add_readme_changelog_entry( $increased_plugin_version, $date );
		}
		if ( file_exists( self::CHANGELOG_TXT )) {
			$this->add_changelog_changelog_entry( $increased_plugin_version, $date );
		}

		return 0;
	}

}
