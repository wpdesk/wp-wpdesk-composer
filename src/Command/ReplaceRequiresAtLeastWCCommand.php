<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Exception;
use stdClass;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Exception\WordpressApiException;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Replaces WC requires at least line in the plugin main file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplaceRequiresAtLeastWCCommand extends BaseCommand
{
    use SedTrait;
    use ColorOutputTrait;

	const PREVIOUS_MINOR_VERSIONS_COUNT = 4;

	protected function configure()
    {
        $this
            ->setName('replace-requires-at-least-wc')
            ->setDescription('Replace a plugin WC requires at least line with a given version.');
    }

	/**
	 * Get all previous WooCommerce versions from WordPress repository sorted by version number descending.
	 *
	 * @return string[]
	 */
	private function wpdesk_get_wc_minor_sorted_versions() {
		$url = 'https://api.wordpress.org/plugins/info/1.0/woocommerce.json';

		$json        = file_get_contents( $url );
		if ( false === $json ) {
			throw new WordpressApiException( 'Unable to load data from Wordpress API' );
		}
		$woocommerce = json_decode( $json, JSON_OBJECT_AS_ARRAY );
		if ( ! is_array( $woocommerce ) || empty( $woocommerce['versions'] ) || ! is_array( $woocommerce['versions'] ) ) {
			throw new WordpressApiException( 'Invalid data from Wordpress API: ' . $json );
		}
		$wc_versions = array_filter( array_keys( $woocommerce['versions'] ), [ $this,
			'remove_beta_versions_and_rc_and_trunk'
		] );

		$wc_versions = $this->get_minor_versions( $wc_versions );

		rsort( $wc_versions );

		return $wc_versions;
	}

	/**
	 * @param string[] $versions_with_patch
	 *
	 * @return string[]
	 */
	private function get_minor_versions( array $versions_with_patch ) {
		$minor_versions = array();

		foreach ( $versions_with_patch as $version_with_patch ) {
			$version_with_patch_exploded = explode( '.', $version_with_patch );
			unset( $version_with_patch_exploded[2] );
			$minor_versions[ implode( '.', $version_with_patch_exploded ) ] = 1;
		}

		return array_keys( $minor_versions );
	}

	/**
	 * @param string $element
	 *
	 * @return bool
	 */
	public function remove_beta_versions_and_rc_and_trunk( $element ) {
		return $element !== 'trunk' && strpos( $element, '-' ) === false;
	}

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	try {
		    $this->replace_wc_requires_at_least( $output );
	    } catch ( WordpressApiException $e ) {
		    $output->write( $this->addColorToOutput( 'WC requires at least not changed!', '0;33' ), true );
		    $output->write( $this->addColorToOutput( 'Exception: ' . $e->getMessage(), '0;33' ), true );
	    }
		return 0;
    }

	protected function replace_wc_requires_at_least( OutputInterface $output ) {
		$extra = $this->getComposer()->getPackage()->getExtra();
		if ( isset( $extra['wc-requires-at-least'] ) ) {
			$wc_requires_at_least = $extra['wc-requires-at-least'];
		} else {
			$wc_versions = $this->wpdesk_get_wc_minor_sorted_versions();
			if ( ! is_array( $wc_versions ) || ! isset( $wc_versions[ self::PREVIOUS_MINOR_VERSIONS_COUNT - 1 ] ) ) {
				throw new WordpressApiException( 'Missing required previous minor version on position ' . self::PREVIOUS_MINOR_VERSIONS_COUNT );
			}
			$wc_requires_at_least = $wc_versions[ self::PREVIOUS_MINOR_VERSIONS_COUNT - 1 ];
		}
		$file_pattern = '*.php';
		$pattern      = "/WC requires at least: [0-9\.]+/";
		$replace      = 'WC requires at least: ' . $wc_requires_at_least;
		$this->file_regex_replace( $file_pattern, $pattern, $replace );
		$output->write( $this->addColorToOutput( 'WC requires at least changed to: ' . $wc_requires_at_least ), true );
	}

}
