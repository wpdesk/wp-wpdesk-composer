<?php
/**
 * @package WPDesk\Composer\GitPlugin\Command
 */

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\LibraryFaker\Faker;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Can fake libraries.
 */
class FakeVendorPrefixedCommand extends BaseCommand
{
    use SedTrait;
    use ExecuteCommandTrait;
	use ColorOutputTrait;

    protected function configure()
    {
        $this
            ->setName('fake-vendor-prefixed')
            ->setDescription('Fakes libraries in the vendor_prefixed dir.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$this->fakeLibrariesIfConfigured();
		return 0;
    }

	/**
	 * Can fake libraries if configured.
	 *
	 * @return void
	 */
	private function fakeLibrariesIfConfigured() {
		/** @see README.md for extra section info. * */
		$extra = $this->getComposer()->getPackage()->getExtra();
		if ( isset( $extra['fake-vendor-prefixed'] ) ) {
			echo $this->addColorToOutput( 'Fake libraries start.' ) . "\n";
			$config = $extra['fake-vendor-prefixed'];
			$faker = new Faker(
				$config['path'] ?? '',
				$config['fake-path'] ?? '',
				$config['content'] ?? [],
				$config['file-names'] ?? [],
				$config['folder-names'] ?? []
			);
			$faker->fake( getcwd() . '/vendor_prefixed' );
			echo $this->addColorToOutput( 'Fake libraries end.' ) . "\n";
		}
	}

}
