<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Exception;
use stdClass;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Replaces PHP requires in the plugin main file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplaceRequiresPHPCommand extends BaseCommand
{
    use SedTrait;

    const PHP_REQUIRES = '7.4';

    protected function configure()
    {
        $this
            ->setName('replace-requires-php')
            ->setDescription('Replace a plugin PHP requires line with a given version.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $extra = $this->getComposer()->getPackage()->getExtra();
        if ( isset($extra['php-requires']) ) {
            $php_requires = $extra['php-requires'];
        } else {
            $php_requires = self::PHP_REQUIRES;
        }

        $value_pattern = "/Requires PHP: ([0-9\.]+)/";
        $php_files     = '*.php';
        $readme_file   = 'readme.txt';

        $php_requires = $this->prepare_current_php_requires_from_file( $php_files, $value_pattern, $php_requires );
        $php_requires = $this->prepare_current_php_requires_from_file( $readme_file, $value_pattern, $php_requires );

        $pattern      = "/Requires PHP: [0-9\.]+/";
        $replace      = 'Requires PHP: ' . $php_requires;
        $this->file_regex_replace( $php_files, $pattern, $replace );
        $this->file_regex_replace( $readme_file, $pattern, $replace );
		return 0;
    }

    /**
     * @param string $files_pattern .
     * @param string $value_pattern .
     * @param string $php_requires .
     * @return string
     */
    protected function prepare_current_php_requires_from_file( $files_pattern, $value_pattern, $php_requires ) {
        $current_php_requires = $this->search_value_by_regex( $files_pattern, $value_pattern );
        if ( ($current_php_requires !== null ) && version_compare( $current_php_requires, $php_requires, '>' ) ) {
            $php_requires = $current_php_requires;
        }
        return $php_requires;
    }
}
