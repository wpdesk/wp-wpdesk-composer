<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Gettext\Merge;
use Gettext\Translations;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use MatthiasMullie\Minify\Minify;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\DirectoriesTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Can minify assets files.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class MinifyAssetsCommand extends BaseCommand
{

    const MINIFY_ASSETS = 'minify-assets';
    const ASSETS_JS = 'js';
    const ASSETS_CSS = 'css';

    use ColorOutputTrait;

    protected function configure()
    {
        $this
            ->setName('minify-assets')
            ->setDescription('Minify JS and CSS assets. There must be configuration in composer.json extra section. See readme file.');
    }

    /**
     * Minify.
     *
     * @param Minify $minify .
     * @param string $file .
     */
    private function minifySingleFile($minify, $file)
    {
        $filename = pathinfo($file);
        $minified_file = $filename['dirname'] . '/' . $filename['filename'] . '.min.' . $filename['extension'];
        $minify->add($file);
        echo "Minifying " . $this->addColorToOutput($file) . " to " . $this->addColorToOutput($minified_file) . "\n";
        $minify->minify($minified_file);
    }

    /**
     * @param string $minify_class .
     * @param array $files .
     */
    private function minify($minify_class, $files)
    {
        foreach ($files as $file) {
            $this->minifySingleFile(new $minify_class(), $file);
        }
    }

    /**
     * Execute command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @see README.md for extra section info. * */
        $extra = $this->getComposer()->getPackage()->getExtra();

        if (isset($extra[self::MINIFY_ASSETS])) {
            $minify_assets = $extra[self::MINIFY_ASSETS];
            if (isset($minify_assets[self::ASSETS_JS])) {
                $this->minify(JS::class, $minify_assets[self::ASSETS_JS]);
            }
            if (isset($minify_assets[self::ASSETS_CSS])) {
                $this->minify(CSS::class, $minify_assets[self::ASSETS_CSS]);
            }
        }
		return 0;
    }
}
