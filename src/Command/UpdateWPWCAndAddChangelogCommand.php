<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\PluginHeaderParserTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\PluginVersionTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;
use WPDesk\Composer\GitPlugin\Command\Tools\Version;

/**
 * Replaces wp tested up line and wc tested line in the plugin readme file and ads changelog.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class UpdateWPWCAndAddChangelogCommand extends BaseCommand {
	use ExecuteCommandTrait;
	use PluginHeaderParserTrait;
	use PluginVersionTrait;
	use SedTrait;

	const README_TXT    = 'readme.txt';
	const CHANGELOG_TXT = 'changelog.txt';

	private $wp_log_entries_readme = [
		'default' => 'Added support for WordPress %1$s',
		'pl'      => 'Dodano wsparcie dla WordPress %1$s',
	];

	private $wc_log_entries_readme = [
		'default' => 'Added support for WooCommerce %1$s',
		'pl'      => 'Dodano wsparcie dla WooCommerce %1$s',
	];

	private $wp_log_entries_changelog = [
		'default' => 'Support for WordPress %1$s',
		'pl'      => 'Wsparcie dla WordPress %1$s',
	];

	private $wc_log_entries_changelog = [
		'default' => 'Support for WooCommerce %1$s',
		'pl'      => 'Wsparcie dla WooCommerce %1$s',
	];

	protected function configure() {
		$this
			->setName( 'update-wp-wc-and-add-changelog' )
			->setDescription( 'Replace a plugin tested up wp and wc line with a last version and add changelog.' );
	}

	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return int|void
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$extra = $this->getComposer()->getPackage()->getExtra();

		$headers_before = $this->parseHeadersForPlugin();

		$this->executePassThrough( 'composer replace-tested-up-wc' );
		$this->executePassThrough( 'composer replace-tested-up-wp' );
		$this->executePassThrough( 'composer replace-requires-at-least-wc' );
		$this->executePassThrough( 'composer replace-requires-at-least-wp' );
		$this->executePassThrough( 'composer replace-requires-php' );

		$headers_after = $this->parseHeadersForPlugin();

		$wp_version_updated = $this->version_updated( $headers_before['tested_up_to'], $headers_after['tested_up_to'] );
		$wc_version_updated = $this->version_updated( $headers_before['wc_tested_up'], $headers_after['wc_tested_up'] );

		if ( $wp_version_updated || $wc_version_updated ) {
			$current_plugin_version   = $headers_before['version'];
			$increased_plugin_version = $this->increase_plugin_version( $current_plugin_version, 2 );

			$this->replace_plugin_version( $increased_plugin_version );

			$lang = $extra['changelog-lang'] ?? 'en';

			$date = date( 'Y-m-d' );

			if ( file_exists( self::README_TXT ) ) {
				$this->add_readme_changelog_entry( $increased_plugin_version, $date, $this->prepare_changelog_entry_for_readme_txt( $wp_version_updated, $wc_version_updated, $headers_after, $lang ) );
			}
			if ( file_exists( self::CHANGELOG_TXT )) {
				$this->add_changelog_changelog_entry( $increased_plugin_version, $date, $this->prepare_changelog_entry_for_changelog_txt( $wp_version_updated, $wc_version_updated, $headers_after, $lang ) );
			}

			$this->write_to_file( '.plugin-version', $increased_plugin_version );
			$this->write_to_file( '.release-notes', $this->prepare_changelog_entry_for_changelog_txt( $wp_version_updated, $wc_version_updated, $headers_after, $lang ) );
		}
		return 0;
	}

	/**
	 * @param string $file_name
	 * @param string $text
	 */
	private function write_to_file( $file_name, $text ) {
		file_put_contents( $file_name, $text );
	}

	/**
	 * @param bool   $wp_version_updated
	 * @param bool   $wc_version_updated
	 * @param array  $headers_after
	 * @param string $lang
	 *
	 * @return string
	 */
	private function prepare_changelog_entry_for_readme_txt( $wp_version_updated, $wc_version_updated, $headers_after, $lang ) {
		$changelog_entry = '';
		if ( $wp_version_updated ) {
			$changelog_entry .= "\n* " . $this->prepare_changelog_entry( $this->wp_log_entries_readme, $headers_after['tested_up_to'], $lang );
		}
		if ( $wc_version_updated ) {
			$changelog_entry .= "\n* " . $this->prepare_changelog_entry( $this->wc_log_entries_readme, $headers_after['wc_tested_up'], $lang );
		}

		return $changelog_entry;
	}

	/**
	 * @param bool   $wp_version_updated
	 * @param bool   $wc_version_updated
	 * @param array  $headers_after
	 * @param string $lang
	 *
	 * @return string
	 */
	private function prepare_changelog_entry_for_changelog_txt( $wp_version_updated, $wc_version_updated, $headers_after, $lang ) {
		$changelog_entry = '### ' . ( $lang === 'pl' ? 'Dodano' : 'Added' );
		if ( $wp_version_updated ) {
			$changelog_entry .= "\n- " . $this->prepare_changelog_entry( $this->wp_log_entries_changelog, $headers_after['tested_up_to'], $lang );
		}
		if ( $wc_version_updated ) {
			$changelog_entry .= "\n- " . $this->prepare_changelog_entry( $this->wc_log_entries_changelog, $headers_after['wc_tested_up'], $lang );
		}

		return $changelog_entry;
	}

	/**
	 * @param array  $log_entries
	 * @param string $version
	 * @param string $lang
	 *
	 * @return bool
	 */
	private function prepare_changelog_entry( $log_entries, $version, $lang ) {
		$changelog_entry = $log_entries[ $lang ] ?? $log_entries['default'];

		return sprintf( $changelog_entry, $version );
	}

	/**
	 * @param string $version_before
	 * @param string $version_after
	 *
	 * @return bool
	 */
	private function version_updated( $version_before, $version_after ) {
		return version_compare( $version_before, $version_after, '<' );
	}

}
