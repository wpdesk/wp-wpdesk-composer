<?php

namespace WPDesk\Composer\GitPlugin\Command\Tools;

/**
 * Can change version.
 *
 * @package WPDesk\Composer\GitPlugin\Command\Tools;
 */
class Version {

	/**
	 * @param string $current_version .
	 * @param string $increase_by .
	 *
	 * @return string
	 */
	public function increase_version( $current_version, $increase_by = '0.1' ) {
		$current_version_exploded = explode( '.', $current_version );
		$increase_by_exploded = explode( '.', $increase_by );
		$increased_version = '';
		foreach ( $current_version_exploded as $position => $version ) {
			if ( '' !== $increased_version && isset( $increase_by_exploded[$position]) ) {
				$increased_version .= '.';
			}
			$version = (int) $version;
			if ( isset( $increase_by_exploded[ $position ] ) ) {
				$version += (int) $increase_by_exploded[ $position ];
				$increased_version .= $version;
			}
		}

		return $increased_version;
	}

}
