<?php

namespace WPDesk\Composer\GitPlugin\Command;


use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;
use WPDesk\Composer\GitPlugin\Command\Tools\Version;

/**
 * Replaces wp tested up line in the plugin readme file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplaceTestedWPCommand extends BaseCommand
{
    use SedTrait;

    const WP_TESTED_UP = '6.2';

    protected function configure()
    {
        $this
            ->setName('replace-tested-up-wp')
            ->setDescription('Replace a plugin tested up wp line with a last version.');
    }


    /**
     * Get latest WP version number from WordPress repository.
     *
     * @return string
     */
    private function wpdesk_get_wp_latest_version() {
        $url = 'http://api.wordpress.org/core/stable-check/1.0/';

        $json         = file_get_contents( $url );
        $all_versions = json_decode( $json );

        $latest_version = self::WP_TESTED_UP;
        foreach ( $all_versions as $version => $status ) {
            if ( 'latest' === $status ) {
                $latest_version = $version;
            }
        }

        return preg_replace( '/([0-9]\.[0-9])(.*)/', '${1}', $latest_version );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ( getenv( 'MINIMAL_WP_TESTED_UP' ) ) {
            $wp_tested_up_to = getenv( 'MINIMAL_WP_TESTED_UP' );
        } else {
            $wp_tested_up_to = self::WP_TESTED_UP;
        }

        $wp_latest_version = $this->wpdesk_get_wp_latest_version();
        if ( version_compare( $wp_tested_up_to, $wp_latest_version, '<' ) ) {
            $wp_tested_up_to = $wp_latest_version;
        }

        try {
            $latest_rc_version = $this->get_latest_rc_version();
            if ( version_compare( $wp_latest_version, $latest_rc_version, '<' ) ) {
                $wp_latest_version = $latest_rc_version;
            }
        } catch ( \Exception $e ) {
            // Just use the latest stable version.
        }

        $file_pattern = 'readme.txt';
        $pattern      = "/Tested up to: [0-9\.]+/";
        $replace      = 'Tested up to: ' . $wp_tested_up_to;
        $this->file_regex_replace( $file_pattern, $pattern, $replace );

        $file_pattern = '*.php';
        $this->file_regex_replace( $file_pattern, $pattern, $replace );
		return 0;
    }

    private function get_latest_rc_version(): string {
        $dom = new \DOMDocument();
        $dom->loadHTMLFile('https://wordpress.org/download/releases/', LIBXML_NOWARNING | LIBXML_NOERROR);
        $xpath = new \DOMXPath($dom);
        $elements = $xpath->query('//div[@id="betas"]//tbody/tr[1]/th');

        [ $latest_rc_version_element ] = $elements;
        $latest_rc_version = $latest_rc_version_element->textContent;

        if ( str_contains( $latest_rc_version, 'RC' ) ) {
            return $this->truncate_patch_version( $latest_rc_version );
        }

        return $this->wpdesk_get_wp_latest_version();
    }

    private function truncate_patch_version( string $version ): string {
        return preg_replace( '/([0-9]\.[0-9])(.*)/', '${1}', $version );
    }
}
