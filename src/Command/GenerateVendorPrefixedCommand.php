<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Path;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

class GenerateVendorPrefixedCommand extends BaseCommand
{
    use SedTrait;
    use ExecuteCommandTrait;

    protected function configure()
    {
        $this
            ->setName('generate-vendor-prefixed')
            ->setDescription('Regenerates the vendor_prefixed dir using vendor.');
    }

    private function isWindows(): bool
    {
        return strcasecmp(substr(PHP_OS, 0, 3), 'WIN') == 0;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$cwd = getcwd();
		$library_path = Path::canonicalize(__DIR__ . '/../../');
		$config = Path::canonicalize(__DIR__ . '/../../res/scoper.inc.php');

		$scoper_command = "{$library_path}/tools/php-scoper add-prefix --working-dir={$cwd} --config={$config} --stop-on-failure --output-dir=./vendor_prefixed --force";

        if ($this->isWindows()) {
            $generateVendorPrefixed = $scoper_command;
        } else {
            $phpBinary = $this->getPHPBinaryPath();
            $generateVendorPrefixed = "{$phpBinary} -d memory_limit=-1 {$scoper_command}";
        }
        $this->executePassThrough($generateVendorPrefixed);
		return 0;
    }
}
