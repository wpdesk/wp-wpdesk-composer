<?php

namespace WPDesk\Composer\GitPlugin\Command\Traits;

/**
 * Trait with a sed like command
 * @see https://pl.wikipedia.org/wiki/Sed_(program)
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
trait SedTrait
{
    /**
     * SED.
     *
     * @param string $file_pattern .
     * @param string|array $pattern .
     * @param string|array $replace .
     *
     * @return string[] array of changed files
     */
    private function file_regex_replace( $file_pattern, $pattern, $replace ) {
        $changed_files = [];

        foreach ( glob( $file_pattern ) as $filename ) {
            $input  = file_get_contents( $filename );
            $output = preg_replace( $pattern, $replace, $input );
            if ( $output !== $input ) {
                $changed_files[] = $filename;
                file_put_contents( $filename, $output );
            }
        }
        return $changed_files;
    }

    /**
     * Finds value in any file using regex.
     *
     * @param string $file_pattern Pattern to search files.
     * @param string $value_pattern Pattern to search value.
     *
     * @return string|null Value found in any file if successful.
     */
    private function search_value_by_regex( $file_pattern, $value_pattern ) {
        foreach ( glob( $file_pattern ) as $filename ) {
            $input = file_get_contents( $filename );
            preg_match( $value_pattern, $input, $matches );
            if ( $matches ) {
                return $matches[1];
            }
        }
        return null;
    }
}
