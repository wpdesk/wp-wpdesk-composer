<?php

namespace WPDesk\Composer\GitPlugin\Command\Traits;

/**
 * Trait with a color output function
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
trait ColorOutputTrait
{
    /**
     * Add color to output.
     *
     * @param string $output
     * @param string $color
     * @return string
     */
    private function addColorToOutput($output, $color = '0;32')
    {
        return "\e[{$color}m{$output}\e[0m";
    }

}