<?php

namespace WPDesk\Composer\GitPlugin\Command\Traits;

/**
 * Plugin version methods.
 */
trait PluginVersionTrait {

	/**
	 * @param string $plugin_version
	 *
	 * @return string
	 */
	private function increase_plugin_version( $plugin_version, $position ) {
		$parsed_version    = explode( '.', $plugin_version );
		$parsed_version[ $position ] = (int) $parsed_version[ $position ] + 1;

		return implode( '.', $parsed_version );
	}

	/**
	 * @param string $plugin_version
	 */
	private function replace_plugin_version( $plugin_version ) {
		$file_pattern = '*.php';
		$pattern      = "/Version: [0-9\.]+/";
		$replace      = 'Version: ' . $plugin_version;

		$this->file_regex_replace( $file_pattern, $pattern, $replace );

		$file_pattern = '*.php';
		$pattern      = "/\\\$plugin_version = \'[0-9\.]+\';/";
		$replace      = '$plugin_version = \'' . $plugin_version . '\';';

		$this->file_regex_replace( $file_pattern, $pattern, $replace );

		$file_pattern = self::README_TXT;
		$pattern      = "/Stable tag: [0-9\.]+/";
		$replace      = 'Stable tag: ' . $plugin_version;

		$this->file_regex_replace( $file_pattern, $pattern, $replace );
	}

	/**
	 * @param string $plugin_version
	 * @param string $date
	 * @param string $changelog_entry
	 */
	private function add_changelog_changelog_entry( $plugin_version, $date, $changelog_entry = 'changelog goes here' ) {
		$first_line   = $this->read_file_first_line( self::CHANGELOG_TXT );

		$pattern = '/' . $this->replace_special_chars_for_regex( $first_line ) . '/';
		$replace = "$first_line" . "\n## [$plugin_version] - $date \n" . $changelog_entry . "\n";

		$this->file_regex_replace( self::CHANGELOG_TXT, $pattern, $replace );
	}

	private function replace_special_chars_for_regex( $str ) {
		$special_chars = [ '.', '+', '*', '?', '^', '$', '(', ')', '[', ']', '{', '}', '|', '/' ];
		foreach ( $special_chars as $special_char ) {
			$str = str_replace( $special_char, '\\' . $special_char , $str );
		}

		return $str;
	}

	/**
	 * @param string $file_name
	 *
	 * @return string
	 */
	private function read_file_first_line( $file_name ) {
		return fgets( fopen( $file_name, 'r' ) );
	}

	/**
	 * @param string $plugin_version
	 * @param string $date
	 * @param string $changelog_entry
	 */
	private function add_readme_changelog_entry( $plugin_version, $date, $changelog_entry = 'changelog goes here' ) {
		$pattern      = "/== Changelog ==/";
		$replace      = '== Changelog ==' . "\n\n= $plugin_version - $date =" . $changelog_entry;

		$this->file_regex_replace( self::README_TXT, $pattern, $replace );
	}

}
