<?php

namespace WPDesk\Composer\GitPlugin\Command\Traits;

/**
 * Trait to execute a command with exit code support
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
trait ExecuteCommandTrait
{
    /**
     * Returns path to php-cli binary
     *
     * @return string
     */
    private function getPHPBinaryPath() {
        return 'php';
    }

    /**
     * Execute a command and pass through output to the stdout. If exit code <> 0 then die completely using given code
     *
     * @param string $command Command to execute
     * @return void
     */
    private function executePassThrough($command)
    {
        passthru($command, $exit_code);
        if ($exit_code !== 0) {
            exit($exit_code);
        }
    }

}