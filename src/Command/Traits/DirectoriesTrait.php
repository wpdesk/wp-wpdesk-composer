<?php

namespace WPDesk\Composer\GitPlugin\Command\Traits;

/**
 * Trait with directories functions
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
trait DirectoriesTrait
{

    /**
     * Get all subdirectories for directory.
     *
     * @param string $dir
     * @param array $skip
     *
     * @return string[]
     */
    private function getDirContents($dir, array $skip = [])
    {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            if (in_array($value, $skip, true)) {
                continue;
            }
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (is_dir($path)) {
                if ($value != "." && $value != "..") {
                    foreach ($this->getDirContents($path) as $subdir) {
                        yield $subdir;
                    }
                }
            }
        }

        yield $dir;
    }

    /**
     * Remove cwd from path.
     *
     * @param string $path
     * @param string $workingDirectory
     * @return string mixed
     */
    private function removeCwdFromPath($path, $workingDirectory)
    {
        return str_replace($workingDirectory, '.', $path);
    }
}