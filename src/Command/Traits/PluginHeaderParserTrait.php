<?php

namespace WPDesk\Composer\GitPlugin\Command\Traits;

/**
 * Can parse plugin header.
 *
 * @see https://github.com/tutv/wp-package-parser/blob/develop/src/class-max-wp-package-parser.php
 */
trait PluginHeaderParserTrait {

	/**
	 * Header map.
	 *
	 * @since 1.0.0
	 * @var array
	 */
	protected $headerMap = [
		'name'                 => 'Plugin Name',
		'plugin_uri'           => 'Plugin URI',
		'version'              => 'Version',
		'description'          => 'Description',
		'author'               => 'Author',
		'author_profile'       => 'Author URI',
		'text_domain'          => 'Text Domain',
		'domain_path'          => 'Domain Path',
		'network'              => 'Network',
		'requires_at_least'    => 'Requires at least',
		'tested_up_to'         => 'Tested up to',
		'wc_requires_at_least' => 'WC requires at least',
		'wc_tested_up'         => 'WC tested up to',
		'requires_php'         => 'Requires PHP'
	];

	/**
	 * Parse the file contents to retrieve its metadata.
	 * Searches for metadata for a file, such as a plugin or theme.  Each piece of
	 * metadata must be on its own line. For a field spanning multiple lines, it
	 * must not have any newlines or only parts of it will be displayed.
	 *
	 * @param string $fileContents File contents. Can be safely truncated to 8kiB as that's all WP itself scans.
	 *
	 * @return array
	 */
	private function parseHeaders( $fileContents ) {
		$headers   = [];
		$headerMap = $this->headerMap;

		//Support systems that use CR as a line ending.
		$fileContents = str_replace( "\r", "\n", $fileContents );

		foreach ( $headerMap as $field => $prettyName ) {
			$found = preg_match( '/^(?:[ \t]*<\?php)?[ \t\/*#@]*' . preg_quote( $prettyName, '/' ) . ':(.*)$/mi', $fileContents, $matches );
			if ( ( $found > 0 ) && ! empty( $matches[1] ) ) {
				//Strip comment markers and closing PHP tags.
				$value             = trim( preg_replace( "/\s*(?:\*\/|\?>).*/", '', $matches[1] ) );
				$headers[ $field ] = $value;
			}
		}

		return $headers;
	}

	/**
	 * @return array
	 */
	private function parseHeadersForPlugin() {
		foreach ( glob( '*.php' ) as $filename ) {
			$headers = $this->parseHeaders( file_get_contents( $filename ) );
			if ( count( $headers ) ) {
				return $headers;
			}
		}

		return [];
	}

}
