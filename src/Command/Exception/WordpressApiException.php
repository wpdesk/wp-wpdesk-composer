<?php

namespace WPDesk\Composer\GitPlugin\Command\Exception;

/**
 * Worpdress API Exception.
 */
class WordpressApiException extends \RuntimeException {
}