<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Composer\Package\Archiver\ArchivableFilesFinder;
use Composer\Package\Archiver\ZipArchiver;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;

/**
 * Can generate plugin .zip file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class GeneratePluginReleaseCommand extends BaseCommand
{

    use ExecuteCommandTrait;

    protected function configure()
    {
        $this
            ->setName('generate-plugin-release')
            ->setDescription('Generates release directory and .zip file with plugin.');
    }

    /**
     * @param string $dir_path .
     */
    private function deleteDirectoryRecursive($dir_path) {
        $files = glob($dir_path . '/{,.}*[!.]', GLOB_MARK | GLOB_BRACE);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteDirectoryRecursive($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir_path);
        return;
    }

    /**
     * @param string $dir_path .
     */
    private function deleteDirIfExists($dir_path) {
        if ( file_exists($dir_path)) {
            $this->deleteDirectoryRecursive($dir_path);
        }
    }

    /**
     * @param string $source_dir .
     * @param string $target_dir .
     * @param array $exclude_files .
     */
    private function copyFiles( $source_dir , $target_dir, $exclude_files = array() ) {
        $files = new ArchivableFilesFinder( $source_dir, $exclude_files );
        foreach ($files as $file) {
            if (!$file->isDir()) {
            /** @var \SplFileInfo $file */
                $rel_path = str_replace($source_dir . '/', '', $file->getRealPath());
                $rel_dir = $target_dir . str_replace($source_dir, '', $file->getPath());
                if (!file_exists($rel_dir)) {
                    mkdir($rel_dir,0777, true);
                }
                copy($file->getRealPath(), $target_dir . '/' . $rel_path);
            }
        }
    }

    /**
     * @param string $source_dir .
     * @param string $tmp_folder .
     */
    private function copyProjectToTmpDir( $source_dir, $tmp_folder ) {
        $this->copyFiles( $source_dir, $tmp_folder, array( '.git', 'release', 'vendor', 'vendor_prefixed' ) );
    }

    /**
     * @param string $tmp_dir .
     * @param string $release_dir .
     */
    private function copyProjectFromTmpDirToReleaseDir( $tmp_dir, $release_dir ) {
        $this->copyFiles(
            $tmp_dir,
            $release_dir,
            array(
                'build-coverage',
                '/release',
                '/tests',
                '/docs',
                '/apigen.neon',
                '/phpunit.xml',
                '/phpcs.xml.dist',
                '/phpunit-integration.xml',
                '/tmp_artifacts',
                '/test_soap.php',
                '/tools',
                '/codeception.dist.yml',
                '/phpcs.xml.dist',
                '/phpunit-integration.xml',
                '/phpunit-unit.xml',
                '/scoper.inc.php',
                'CHANGELOG.md',
                'README.md',
                'LICENSE.md',
                'LICENSE',
                '.git',
                '.editorconfig',
                '.gitignore',
                '.gitlab-ci.yml',
                '.gitlab',
                'composer.json',
                '.tmp',
                '.env.testing',
                'phpstan.dist',
                'composer.lock',
                '/phpstan.neon.dist',
                'pl_PL.mo',
                'pl_PL.po',
            )
        );
    }

    /**
     * @param string $release_dir
     * @param string $zip_file
     * @param OutputInterface $output
     */
    private function generateReleaseZip( $release_dir, $zip_file, OutputInterface $output ) {
        $archiver    = new ZipArchiver();
        $archiveFile = $archiver->archive(
            $release_dir,
            $zip_file,
            'zip'
        );
        $output->writeln("Release zip created {$zip_file}");
    }

    /**
     * Execute command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = getcwd();
        $release_dir = $source . '/release';
        $zip_file = $release_dir . '/' . basename($source) . '.zip';
        $tmp_dir = sys_get_temp_dir() . '/wpdesk_release' . uniqid();
        mkdir($tmp_dir);

        $this->deleteDirIfExists( $release_dir );
        $release_dir = $release_dir . '/' . basename($source);
        mkdir($release_dir,0777, true);

        $this->copyProjectToTmpDir( $source, $tmp_dir );

        exec("cd {$tmp_dir} && COMPOSER_MEMORY_LIMIT=-1 && composer install -d {$tmp_dir}");
        exec("cd {$tmp_dir} && COMPOSER_MEMORY_LIMIT=-1 && composer install --no-dev --no-progress --optimize-autoloader --prefer-dist");

        $this->copyProjectFromTmpDirToReleaseDir( $tmp_dir, $release_dir );

        $this->deleteDirIfExists( $tmp_dir );

        $output->writeln("Release folder created {$release_dir}");

        $this->generateReleaseZip( $release_dir, $zip_file, $output );
		return 0;
    }
}
