<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Symfony\Component\Console\Input\InputArgument;

/**
 *
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class RunPhpUnitIntegrationTestsCommand extends RunPhpUnitTestsCommand
{

	/**
	 * Configure command.
	 */
	protected function configure()
	{
		$this
			->setName('run-integration-tests')
			->setDescription('Run PHPUnit integration tests.')
			->setDefinition(
				array(
					new InputArgument(
						self::FAST,
						InputArgument::OPTIONAL,
						'Fast tests - do not shutdown docker-compose.',
						'slow'
					),
				)
			)
		;
	}

	/**
	 * Get Doceker Compose command.
	 *
	 * @return string
	 */
	protected function getDockerComposeCommand() {
		$project_name = basename( getcwd() );
		return 'bash -c "' .
		       'vendor/wpdesk/wp-wpdesk-composer/docker/wait' .
		       ' && ln -s /project /tmp/wordpress-develop/src/wp-content/plugins/' . $project_name .
		       ' && cd /tmp/wordpress-develop/src/wp-content/plugins/' . $project_name .
		       ' && vendor/bin/phpunit  -d memory_limit=-1 --configuration phpunit-integration.xml --no-coverage --log-junit tmp_artifacts/report.xml' .
		       '"';
	}

	/**
	 * Get service name.
	 *
	 * @return string
	 */
	protected function getServiceName() {
		return 'integration';
	}

}