<?php

namespace WPDesk\Composer\GitPlugin\Command;


use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Replaces contributors line in the plugin readme file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplaceContributorsCommand extends BaseCommand
{
    use SedTrait;

    const CONTRBUTORS_ARGUMENT = 'contributors';
    const FILE_ARGUMENT = 'readme';

    const DEFAULT_CONTRIBUTORS = 'wpdesk,dyszczo,grola,piotrpo,marcinkolanko,mateuszgbiorczyk,sebastianpisula,bartj';
    const DEFAULT_FILE = 'readme.txt';

    protected function configure()
    {
        if ( getenv( 'CONTRIBUTORS' ) ) {
            $contributors = getenv( 'CONTRIBUTORS' );
        } else {
            $contributors = self::DEFAULT_CONTRIBUTORS;
        }

        $this
            ->setName('replace-contributors')
            ->setDescription('Generates a contributors in a WordPress plugin readme file.')
            ->setDefinition(array(
                new InputArgument(self::FILE_ARGUMENT, InputArgument::OPTIONAL, 'WordPress document file to parse',
                    self::DEFAULT_FILE),
                new InputArgument(self::CONTRBUTORS_ARGUMENT, InputArgument::OPTIONAL, 'List of contributors delimited by comma', $contributors),
            ))
            ->setHelp(
                <<<EOT
Executes sed command on the readme file to put repository contributors in place.
EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $contributors = $input->getArgument(self::CONTRBUTORS_ARGUMENT);
        $file_pattern = $input->getArgument(self::FILE_ARGUMENT);
        $pattern      = "/Contributors: (.+?)\n/";
        $replace      = 'Contributors: ' . $contributors . "\n";

        $this->file_regex_replace( $file_pattern, $pattern, $replace );
		return 0;
    }
}
