<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Symfony\Component\Console\Input\InputArgument;

/**
 *
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class RunPhpUnitUnitTestsCommand extends RunPhpUnitTestsCommand
{

	/**
	 * Configure command.
	 */
	protected function configure()
	{
		$this
			->setName('run-unit-tests')
			->setDescription('Run PHPUnit unit tests.')
			->setDefinition(
				array(
					new InputArgument(
						self::FAST,
						InputArgument::OPTIONAL,
						'Fast tests - do not shutdown docker-compose.',
						'slow'
					),
				)
			)
		;
	}

	/**
	 * Get Doceker Compose command.
	 *
	 * @return string
	 */
	protected function getDockerComposeCommand() {
		return 'vendor/bin/phpunit  -d memory_limit=-1 --configuration phpunit-unit.xml --no-coverage --log-junit tmp_artifacts/report.xml';
	}

	/**
	 * Get service name.
	 *
	 * @return string
	 */
	protected function getServiceName() {
		return 'unit';
	}

}