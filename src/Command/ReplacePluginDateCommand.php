<?php

namespace WPDesk\Composer\GitPlugin\Command;


use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Replaces plugin release date in all php files in the dir
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplacePluginDateCommand extends BaseCommand
{
    use SedTrait;

    protected function configure()
    {
        $this
            ->setName('replace-plugin-date')
            ->setDescription('Replace a plugin release date in every php file found in dir. The date used by the command is the composer.lock last modified date so the libraries will always be correct.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $release_date = date( 'Y-m-d H:i', filemtime('composer.lock') );

        $file_pattern = '*.php';
        $pattern      = "/\\\$plugin_release_timestamp = \'(.+?)\';/";
        $replace      = '$plugin_release_timestamp = \'' . $release_date . '\';';

        $this->file_regex_replace( $file_pattern, $pattern, $replace );
		return 0;
    }
}
