<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Composer\Factory;
use Gettext\Translations;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\DirectoriesTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;

/**
 * Can generate plugin .pot file.
 * File fill be generated in folder configured in composer.json.
 * Existing file fill be overwritten.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class GeneratePotCommand extends BaseCommand {

	use ColorOutputTrait;
	use DirectoriesTrait;
	use ExecuteCommandTrait;

	const TEXT_DOMAIN = 'text-domain';
	const TRANSLATIONS_FOLDER = 'translations-folder';

	protected function configure() {
		$this
			->setName( 'generate-pot' )
			->setDescription( 'Generates .pot file for translations. Existing file will be overwritten. There must be configuration in composer.json extra section. See readme file.' );
	}

	/**
	 * Execute command.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return int|void|null
	 */
	protected function execute( InputInterface $input, OutputInterface $output )
	{
		$command = './vendor/bin/wp i18n make-pot . ' . $this->prepare_pot_file_name() . ' --allow-root';
		echo 'Generating .pot file: ' . $this->addColorToOutput($command) . "\n";
		$this->executePassThrough( $command );
        return 0;
	}

	private function prepare_pot_file_name() {
		$extra = $this->getComposer()->getPackage()->getExtra();

		if ( isset( $extra[ self::TEXT_DOMAIN ] ) ) {
			$working_directory = getcwd();
			$translations_folder = isset( $extra[ self::TRANSLATIONS_FOLDER ] ) ? $extra[ self::TRANSLATIONS_FOLDER ] : 'lang';
			$plugin_text_domain = $extra[self::TEXT_DOMAIN];

			return $working_directory . '/' . $translations_folder . '/' . $plugin_text_domain . '.pot';
		}

		return '';
	}

}
