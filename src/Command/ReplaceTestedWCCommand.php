<?php

namespace WPDesk\Composer\GitPlugin\Command;


use Composer\Command\BaseCommand;
use Exception;
use stdClass;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;
use WPDesk\Composer\GitPlugin\Command\Tools\Version;

/**
 * Replaces wc tested up line in the plugin readme file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplaceTestedWCCommand extends BaseCommand {
	use SedTrait;

	const WC_TESTED_VERSION = '7.6';

	protected function configure() {
		$this
			->setName( 'replace-tested-up-wc' )
			->setDescription( 'Replace a plugin tested up wc line with a last version.' );
	}

	/**
	 * @return string
	 */
	private function get_api_url() {
		return 'https://api.wordpress.org/plugins/info/1.0/woocommerce.json';
	}


	/**
	 * Get latest WooCommerce version number from WordPress repository.
	 *
	 * @return string
	 */
	private function get_wc_current_version() {
		$minimal_version = self::WC_TESTED_VERSION;
		try {
			$json        = file_get_contents( $this->get_api_url() );
			$woocommerce = json_decode( $json );
		} catch ( Exception $e ) {
			$woocommerce          = new stdClass();
			$woocommerce->version = $minimal_version;
		}

		return $this->truncate_patch_version( $woocommerce->version );
	}

	/**
	 * @return string
	 */
	private function get_latest_beta_or_rc_version() {
		$version = '1.0.0';
		try {
			$json        = file_get_contents( $this->get_api_url() );
			$woocommerce = json_decode( $json, JSON_OBJECT_AS_ARRAY );
			foreach ( $woocommerce['versions'] as $version_with_beta => $version_zip ) {
				$version_exploded = explode( '-', $version_with_beta );
				$version          = version_compare( $version, $version_exploded[0], '<' ) ? $version_exploded[0] : $version;
			}
		} catch ( Exception $e ) {
			return $this->get_wc_current_version();
		}

		return $this->truncate_patch_version( $version );
	}

	/**
	 * @param string $version
	 *
	 * @return string
	 */
	private function truncate_patch_version( $version ) {
		return preg_replace( '/([0-9]\.[0-9])(.*)/', '${1}', $version );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		if ( getenv( 'MINIMAL_WC_TESTED_UP' ) ) {
			$wc_tested_up_to = getenv( 'MINIMAL_WC_TESTED_UP' );
		} else {
			$wc_tested_up_to = self::WC_TESTED_VERSION;
		}

		$wc_latest_version = $this->get_wc_current_version();
		if ( version_compare( $wc_tested_up_to, $wc_latest_version, '<' ) ) {
			$wc_tested_up_to = $wc_latest_version;

		}
		$wc_latest_beta_or_rc_version = $this->get_latest_beta_or_rc_version();
		if ( version_compare( $wc_tested_up_to, $wc_latest_beta_or_rc_version, '<' ) ) {
			$wc_tested_up_to = $wc_latest_beta_or_rc_version;
		}

		if ( getenv( 'MAX_MINIMAL_WC_TESTED_UP' ) ) {
			$max_wc_tested_up_to = getenv( 'MAX_MINIMAL_WC_TESTED_UP' );
			if ( version_compare( $max_wc_tested_up_to, $wc_tested_up_to, '<' ) ) {
				$wc_tested_up_to = $max_wc_tested_up_to;
			}
		}

		$file_pattern = '*.php';
		$pattern      = "/WC tested up to: [0-9\.]+/";
		$replace      = 'WC tested up to: ' . $wc_tested_up_to;

		$this->file_regex_replace( $file_pattern, $pattern, $replace );
		return 0;
	}

}
