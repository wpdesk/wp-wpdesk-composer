<?php

namespace WPDesk\Composer\GitPlugin\Command\LibraryFaker;

use WPDesk\Composer\GitPlugin\Command\Traits\DirectoriesTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Can fake libraries.
 */
class Faker
{
	use DirectoriesTrait;
	use SedTrait;

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var string
	 */
	private $fake_path;

	/**
	 * @var array<string, string>
	 */
	private $content;

	/**
	 * @var array<string, string>
	 */
	private $file_names;

	/**
	 * @var array<string, string>
	 */
	private $folder_names;

	/**
	 * @param string $path
	 * @param string $fake_path
	 * @param array<string, string> $content
	 * @param array<string, string> $file_names
	 * @param array<string, string> $folder_names
	 */
	public function __construct($path, $fake_path, array $content, array $file_names, array $folder_names)
	{
		$this->path = $path;
		$this->fake_path = $fake_path;
		$this->content = $content;
		$this->file_names = $file_names;
		$this->folder_names = $folder_names;
	}

	/**
	 * @param array $config
	 *
	 * @return Faker
	 */
	public static function createFromConfig(array $config) {
		return new Faker(
			$config['path'] ?? '',
			$config['fake-path'] ?? '',
			$config['content'] ?? [],
			$config['file-names'] ?? [],
			$config['folder-names'] ?? []
		);
	}

	/**
	 * @param string $working_directory
	 *
	 * @return void
	 */
	public function fake($working_directory) {
		if (!empty($this->path)) {
			$path = $working_directory . '/' . $this->path;
			if (!empty($this->content)) {
				$this->fakeContent($path);
			}
			if (!empty($this->file_names)) {
				$this->fakeFileNames($path);
			}
			if (!empty($this->folder_names)) {
				$this->fakeFolderNames($path);
			}
			if (!empty($this->fake_path)) {
				rename($path, $working_directory . '/' . $this->fake_path);
			}
		}
	}

	/**
	 * @param string $repository
	 *
	 * @return string
	 */
	public function fakePrefixedRepositoryName($repository) {
		$directory = dirname($repository);
		$repository_name = basename($repository);
		if (!empty($this->path) && !empty($this->fake_path) && $this->path !== $this->fake_path && $this->path === $directory) {
			$directory = $this->fake_path;
		}
		if (!empty($this->folder_names)) {
			$repository_name = $this->fakeSingleName($repository_name, $this->folder_names);
		}

		return $directory . '/' . $repository_name;
	}

	/**
	 * @param string $path
	 *
	 * @return void
	 */
	private function fakeContent($path) {
		foreach ( $this->getDirContents($path) as $dir) {
			$file_pattern = $dir . '/*.*';
			$this->fakeContentInFiles($file_pattern);
		}
	}

	/**
	 * @param string $file_pattern
	 *
	 * @return void
	 */
	private function fakeContentInFiles($file_pattern) {
		$patterns = [];
		$replacements = [];
		foreach ($this->content as $find=>$replace) {
			$patterns[] = "/$find/";
			$replacements[] = $replace;
		}
		$this->file_regex_replace($file_pattern, $patterns, $replacements);
	}

	/**
	 * @param string $path
	 *
	 * @return void
	 */
	private function fakeFileNames($path) {
		foreach ($this->getDirContents($path) as $dir) {
			$file_pattern = $dir . '/*.*';
			foreach (glob( $file_pattern ) as $filename) {
				$name = basename($filename);
				$new_name = $this->fakeSingleName($name, $this->file_names);
				if ($name !== $new_name) {
					echo "Rename file $name to $new_name\n";
					rename( $dir . '/' . $name, $dir . '/' . $new_name );
				}
			}
		}
	}

	/**
	 * @param string $name
	 * @param array<string, string> $names
	 *
	 * @return string
	 */
	public function fakeSingleName($name, $names) {
		foreach ($names as $find=>$replace) {
			$name = str_replace($find, $replace, $name);
		}
		return $name;
	}

	/**
	 * @param string $path
	 *
	 * @return void
	 */
	private function fakeFolderNames($path) {
		foreach ($this->getDirContents($path) as $dir) {
			if ($dir === $path) {
				continue;
			}
			$name = basename($dir);
			$new_name = $this->fakeSingleName($name, $this->folder_names);
			if ($name !== $new_name) {
				$new_dir = dirname($dir) . '/' . $new_name;
				echo "Rename folder $dir to $new_dir\n";
				rename($dir, $new_dir);
				$this->fakeFolderNames($path);
				break;
			}
		}
	}

}
