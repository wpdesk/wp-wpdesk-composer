<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\ColorOutputTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\DirectoriesTrait;
use WPDesk\Composer\GitPlugin\Command\Traits\ExecuteCommandTrait;

/**
 * Can generate .phpstorm.meta.php directory and files.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class GeneratePhpStormMeta extends BaseCommand {

	const TEXT_DOMAIN = 'text-domain';

	protected function configure() {
		$this
			->setName( 'generate-phpstorm-meta' )
			->setDescription( 'Generates PhpStorm META files, for better codding ;p.' );
	}

	/**
	 * Execute command.
	 *
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return int|void|null
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		$extra = $this->getComposer()->getPackage()->getExtra();

		if ( isset( $extra[ self::TEXT_DOMAIN ] ) ) {
			$working_directory = getcwd();
			$file              = $working_directory . '/' . '.phpstorm.meta.php';

			if ( file_exists( $file ) ) {
				return 0;
			}

			$plugin_text_domain_file_contents = file_get_contents( __DIR__ . '/.phpstorm.meta.php/plugin-text-domain.php' );
			$plugin_text_domain_file_contents = str_replace( '$plugin_text_domain', $extra[ self::TEXT_DOMAIN ], $plugin_text_domain_file_contents );

			file_put_contents( $file, $plugin_text_domain_file_contents );
		}

		return 0;
	}

}
