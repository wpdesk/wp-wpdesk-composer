<?php

namespace WPDesk\Composer\GitPlugin\Command;

use Composer\Command\BaseCommand;
use Exception;
use stdClass;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WPDesk\Composer\GitPlugin\Command\Traits\SedTrait;

/**
 * Replaces WP requires at least line in the plugin main file.
 *
 * @package WPDesk\Composer\GitPlugin\Command
 */
class ReplaceRequiresAtLeastWPCommand extends BaseCommand
{
    use SedTrait;

    const WP_REQUIRES_AT_LEAST = '6.4';

    protected function configure()
    {
        $this
            ->setName('replace-requires-at-least-wp')
            ->setDescription('Replace a plugin WP requires at least line with a given version.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $extra = $this->getComposer()->getPackage()->getExtra();
        if ( isset($extra['wp-requires-at-least']) ) {
            $wp_requires_at_least = $extra['wp-requires-at-least'];
        } else {
            $wp_requires_at_least = self::WP_REQUIRES_AT_LEAST;
        }

        $file_pattern = '*.php';
        $pattern      = "/Requires at least: [0-9\.]+/";
        $replace      = 'Requires at least: ' . $wp_requires_at_least;

        $this->file_regex_replace( $file_pattern, $pattern, $replace );
		return 0;
    }
}
