## [3.0.2] - 2024-09-26
### Fixed
- Stripped vendor name from seeking path, if needed when replacing translation domain in libraries.

## [3.0.1] - 2024-09-16
### Fixed
- Exposed missing WP Desk shared classes.

## [3.0.0] - 2024-09-16
### Changed
- PSR libraries are now ALWAYS prefixed. Should you need to disable it, configure it directly in your `scoper.inc.php` file.
- Removed scoping with director in favour of using php-scoper directly from the library. Update php-scoper and remove the process of error-prone rewriting the scoper internals. Try to handle config migration for php-scoper gracefully, with the aid of internal config helper.
- For plugins using automated commands, raised minimal required PHP version to 7.4 and minimal required WordPress version to 6.4.
- Raised minimal PHP version to 7.2.

## [2.27.1] - 2024-04-22
### Changed
- When updating WordPress compatibility with `replace-tested-up-wp`, use latest release candidate as potential reference to version bump.

## [2.27.0] - 2024-03-01
### Added
- Generate `l10n.php` files ready for WordPress 6.5 internalization improvements.

## [2.26.0] - 2023-11-21

### Changed
- Deprecated `phpstan` installation through director. Use `wpdesk/phpstan-rules` package in your plugin.

### Removed
- Removed `phpcs` related packages from director.

## [2.25.1] - 2023-07-06
### Fixed
- Psr\Log prefixer - added missing classes

## [2.25.0] - 2023-06-13
### Removed
- Automatic plugin header update, this should be done manually by running `composer update-wp-wc-and-add-changelog`

## [2.24.2] - 2023-06-12
### Changed
- Use the latest version of WP and WC stubs for director

## [2.24.1] - 2023-03-27
## Changed
- WC tested up 7.6
- WP tested up 6.2

## [2.24.0] - 2023-02-14
## Added
- increase-plugin-version command

## [2.23.1] - 2023-02-08
## Fixed
- PhpStorm meta generation form plugin text domain

## [2.23.0] - 2022-12-20
## Changed
- Requires at least 5.8
- Required PHP 7.2

## [2.22.1] - 2022-12-12
## Changed
- Removed `post_checkout` and `post_merge` hooks
- If director is already installed, don't repeat installation

## [2.22.0] - 2022-11-28
## Added
- PhpStorm meta generation form plugin text domain

## [2.21.3] - 2022-11-28
## Changed
- Updated `wp-cli` packages to version ~2.7

## [2.21.2] - 2022-10-19
## Changed
- versions in update-wp-wc-and-add-changelog command

## [2.21.1] - 2022-10-18
## Changed
- WordPress typo

## [2.21.0] - 2022-10-13
## Changed
- WC tested up 7.0
- WP tested up 6.1

## [2.20.2] - 2022-10-11
## Fixed
- generate pot

## [2.20.1] - 2022-09-29
## Fixed
- .pot file generation

## [2.20.0] - 2022-08-30
## Added
- availability to prefix Psr

## [2.19.0] - 2022-07-19
## Changed
- Requires at least 5.7

## [2.18.0] - 2022-05-27
## Added
- Library Faker

## [2.17.10] - 2022-05-04
## Changed
- Locked version of php-stubs/woocommerce-stubs on 6.3.1 (CouponsMovedTrait not found)

- ## [2.17.9] - 2022-05-04
## Changed
- WC tested up 6.5
- WP tested up 6.0

## [2.17.8] - 2022-04-05
## Fixed
- Restore changes from composer.json

[2.17.6] - 2022-04-04
## Fixed
- npm ci instead of npm install: https://docs.npmjs.com/cli/v8/commands/npm-ci

## [2.17.5] - 2022-01-20
## Fixed
- generate pot

## [2.17.4] - 2021-12-20
## Changed
- WC tested up 6.0
- WP tested up 5.9

## [2.17.3] - 2021-12-08
## Fixed
- execute returns 0

## [2.17.2] - 2021-11-09
## Fixed
- update-wp-wc-and-add-changelog - separate entries for readme.txt and changelog.txt

## [2.17.1] - 2021-10-25
## Fixed
- update-wp-wc-and-add-changelog - check if readme.txt or changelog.txt exists

## [2.17.0] - 2021-10-25
## Added
- update-wp-wc-and-add-changelog command

## [2.16.0] - 2021-10-21
## Changed
- Reverted: Tested versions of WP and WC increased by 0.1 of the current versions

## [2.15.2] - 2021-09-11
## Fixed
- Fix broken support for standard git

## [2.15.1] - 2021-09-07
## Fixed
- Fix support git worktrees

## [2.15.0] - 2021-08-11
## Changed
- WC requires at least depends on current WC version

## [2.14.7] - 2021-07-20
## Changed
- Tested versions of WP and WC increased by 0.1 of the current versions

## [2.14.6] - 2021-07-16
## Changed
- gettext locked on 4.8.4

## [2.14.5] - 2021-07-11
## Changed
- WC tested up to: 5.5
- WC requires at least: 5.2
- WP tested up 5.8

## [2.14.4] - 2021-06-07
## Changed
- Locked wp-cli-bundle on version 2.4.0 and wp-cli on version 2.4.1, newer versions fails on translations: https://gitlab.com/wpdesk/flexible-shipping/-/jobs/1323303555

## [2.14.0] - 2021-05-25
## Changed
- Disable contributors replace on commit

## [2.13.1] - 2021-05-18
## Fixed
- Does not replace hook files

## [2.13.0] - 2021-04-29
## Changed
- Better PHPStan stubs
- Better scoper

## [2.12.3] - 2021-04-28
## Changed
- exit(1) when director cannot be initialized
## Fixed
- "i18n' is not a registered wp command"

## [2.12.2] - 2021-04-28
## Fixed
- Required PHP 7.3 message

## [2.12.1] - 2021-04-27
## Fixed
- Required PHP 7.3

## [2.12.0] - 2021-04-26
## Changed
- Scoper version updated from 0.12.4 to ^0.14
- Reflector updated to support PHP 8
- New WordPress stubs & WooCommerce stubs
- Jetbrains stubs from dev-master (for now)
- Required PHP 7.3
## Fixed
- Support for PHP 8 in director
- Support for Composer 2 in director

## [2.11.0] - 2021-04-21
## Added
- Support for Composer 2
- Support for PHP 8

## [2.10.5] - 2021-04-13
## Changed
- WC tested up to: 5.3
- WC requires at least: 4.8

## [2.10.4] - 2021-03-03
## Changed
- WC tested up to: 5.2
- WC requires at least: 4.7
- WP tested up to: 5.7

## [2.10.3] - 2021-02-23
## Fixed
- Translations for plurals

## [2.10.2] - 2021-02-22
## Removed
- --pretty-print from JS translations JSON
- npm purge

## [2.10.1] - 2021-02-22
## Added
- --allow-root in wp-cli

## [2.10.0] - 2021-02-21
## Added
- JavaScript translations

## [2.9.9] - 2021-02-10
## Changed
- WC tested up to: 5.1

## [2.9.8] - 2021-01-19
## Changed
- Added gettext functions for generate-pot action

## [2.9.7] - 2021-01-11
## Changed
- WC tested up to: 5.0
- WC requires at least: 4.6

## [2.9.6] - 2020-12-10
## Fixed
- POT generator: WP gettext functions

## [2.9.5] - 2020-12-10
## Changed
- mateuszgbiorczyk in contributors

## [2.9.4] - 2020-12-10
## Changed
- WC tested up to: 4.9
- WC requires at least: 4.5

## [2.9.3] - 2020-11-26
## Changed
- WP tested up to: 5.6
- WC tested up to: 4.8

## [2.9.2] - 2020-11-18
## Changed
- WC requires at least: 4.4

## [2.9.1] - 2020-10-26
## Fixed
- npm on windows

## [2.9.0] - 2020-10-20
## Added
- composer npm command

## [2.8.12] - 2020-09-24
## Fixed
- replace-requires-php command for readme.txt file

## [2.8.11] - 2020-09-21
## Added
- replace-requires-php command

## [2.8.10] - 2020-09-16
## Fixed
- Rounding of tested WC version to minor version
## Changed
- Rounding of tested WP version to minor version

## [2.8.9] - 2020-08-27
## Changed
- WC requires at least: 4.2
## Added
- replace-requires-at-least-wp command

## [2.8.8] - 2020-08-14
## Changed
- WC_VERSION should not be prefixed

## [2.8.7] - 2020-08-03
## Changed
- WP tested up to: 5.5

## [2.8.6] - 2020-07-30
## Changed
- WC requires at least: 4.0

## [2.8.5] - 2020-06-02
## Changed
- tested WC version - added 1 to minor in current WC version
## Added
- support for MAX_MINIMAL_WC_TESTED_UP env variable

## [2.8.4] - 2020-05-07
## Added
- replace-requires-at-least-wc command

## [2.8.3] - 2020-05-06
## Changed
- damianmachnik is a supporter not dev

## [2.8.2] - 2020-05-05
## Changed
- Minimal WC version: 4.2

## [2.8.1] - 2020-05-02
## Changed
- Contributors changed to wpdesk,dyszczo,grola,piotrpo,dwukropek,marcinkolanko,damianmachnik

## [2.8.0] - 2020-04-27
### Added
- generate-plugin-release command
### Removed
- create-plugin-package command

## [2.7.1] - 2020-04-17
### Fixed
- windows bug with cygwin

## [2.7.0] - 2020-04-14
### Added
- wpdesk/wp-code-sniffer library

## [2.6.4] - 2020-04-02
### Changed
- dwukropek removed from contributors

## [2.6.3] - 2020-03-19
### Changed
- Minimal WC version: 4.1
- Minimal WP version: 5.4

## [2.6.1] - 2020-03-03
### Changed
- Minimal WC version: 4.0

## [2.6.0] - 2020-01-29
## Added
- PSR-11 ContainerInterface added to interoperability classes

## [2.5.2] - 2020-01-22
## Changed
- Remove disabled translations

## [2.5.0] - 2020-01-14
## Changed
- Contributors changed to wpdesk,dyszczo,grola,piotrpo,dwukropek,marcinkolanko

## [2.4.9] - 2019-12-17
### Changed
- Minimal WC version: 3.9

## [2.4.8] - 2019-12-09
### Fixed
- memory limit in phpstan
### Removed
- composer.lock check in .gitignore as there was some issues in libraries

## [2.4.7] - 2019-11-26
### Added
- added minification functionality

## [2.4.6] - 2019-11-14
### Added
- added LoggerTrait to MISSING_CLASSES

## [2.4.5] - 2019-10-30
### Fixed
- working directory in translations

## [2.4.4] - 2019-10-30
### Added
- merge translations should not fire with --no-dev flag

## [2.4.3] - 2019-10-31
### Changed
- command classes replace
- removed php-scoper memory limit
### Added
- exit code in scoper when failed

## [2.4.2] - 2019-10-30
### Changed
- yield from

## [2.4.1] - 2019-10-30
### Changed
- removed wp-cli and used getext to generate .POT file

## [2.4.0] - 2019-10-28
### Added
- translations commands

## [2.3.2] - 2019-10-17
### Changed
- Minimal WC version: 3.8
- Minimal WP version: 5.3

## [2.3.0] - 2019-09-12
### Added
- Psr cache as interoperability classes

## [2.2.2] - 2019-09-02
## Changed
- Contributors changed to wpdesk,dyszczo,grola,potreb

## [2.2.1] - 2019-08-19
### Added
- PHPUnit tests run commands

## [2.2.0] - 2019-08-13
### Added
- WPDesk_Tracker_Data_Provider, WPDesk_Tracker_Interface, WPDesk_Tracker_Sender as interoperability classes
- PHPCS hook runs only when phpcs.xml.dist file exists
### Changed
- Minimal WC version: 3.7

## [2.1.3] - 2019-08-12
### Fixed
- remove hooks only when exists

## [2.1.2] - 2019-08-09
### Changed
- vendor_prefixed dir should be initialized
### Added
- phpcs.xml.dist example

## [2.1.0] - 2019-08-08
### Changed
- Director can detect php version and will just show a message then
- PHPScoper will not automatically start when scope config is not present
- New scoper config example

## [2.0.7] - 2019-08-01
### Added
- WP_MEMORY_LIMIT const whitelisted

## [2.0.6] - 2019-07-31
### Added
- WordPress config consts whitelisted

## [2.0.5] - 2019-07-30
### Added
- SCRIPT_DEBUG in whitelisted consts
- WC() function whitelisted
- Psr Null logger whitelisted
- WPDesk_Tracker_Factory whitelisted - for prefixer hack

## [2.0.0] - 2019-07-23
### Added
- Prefixer amd vendor_prefixed command
### Changed
- Regenerate vendor_prefixed after update

## [1.2.0] - 2019-07-19
### Added
- Remove hooks on disable
- WC stubs for PHPStan
- Default config for PHPStan

## [1.1.0] - 2019-07-16
### Added
- Embedded additional 'director' project with PHPCS and PHPStan in different vendor dir
### Changed
- Removed additional dev requirements
### Fixed
- Removed info for WIN not supported
- Error for not existent files when --no-dev used

## [1.0.2] - 2019-07-16
### Changed
- Additional dev requirements only when CI is not used

## [1.0.1] - 2019-07-15
### Added
- Init
