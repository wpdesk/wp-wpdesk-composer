<?php

namespace unit\Command\Tools;

use WP_Mock\Tools\TestCase;
use WPDesk\Composer\GitPlugin\Command\Tools\Version;

class VersionTest extends TestCase {

	/**
	 * @var \PHPUnit\Framework\MockObject\MockObject|Version
	 */
	private $version;

	public function setUp(): void {
		parent::setUp();

		$this->version = new Version();
	}

	public function testShouldIncreaseVersionWithDefaultIncreaseByValue() {
		// When
		$actual = $this->version->increase_version( '1.7' );

		// Then
		$this->assertEquals( '1.8', $actual );
	}

	public function testShouldIncreaseVersionWithDefaultIncreaseBy1() {
		// When
		$actual = $this->version->increase_version( '1.7', '1' );

		// Then
		$this->assertEquals( '2', $actual );
	}

}
